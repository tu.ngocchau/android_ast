# android_ast

AST for android (java + android)

This project is an Android version of javalang project provided by Christopher Thunes

https://github.com/c2nes/javalang

For the experiments, please go to the folder while in jupyter notebook 
The android_tokenizer.py is in src folder
In the same folder, the android_package_tokenizer.py (discussion points) is used to provide Package-based Identifier
