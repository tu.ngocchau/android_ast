import unittest
from .. import tokenizer

class TestAndroidTokenizer(unittest.TestCase):

     def test_tokenizer_javadoc(self):
        # Same 
        code = "/**\n" \
               " * See {@link BlockTokenSecretManager#setKeys(ExportedBlockKeys)}\n" \
               " */"

        # When
        tokens = list(tokenizer.tokenize(code))

        # Then
        self.assertEqual(len(tokens), 0)

     def test_tokenizer_androidCode(self):
        # Same 
        code = "public void closeSubmit(String p3)" \
               "{"\
               "android.widget.Toast.makeText(this.a, p3, 1).show();"\
               "((android.app.Activity) this.a).finish();"\
               "return;" \
               "}"\

        tokens = list(tokenizer.tokenize(code))
