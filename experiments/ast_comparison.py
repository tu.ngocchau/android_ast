import os
import sys
import pandas as pd
import numpy as np

class astComparison(object):
      """Compare aast and ast in the source code."""

      def __init__(self, source_name):
            self.data = None
            self.aast_vol = {}
            self.ast_vol = {}
            self.data_path = os.path.join("data", source_name)
            self.source_name = source_name

      def dataLoad(self, limit=1):
            df_count = 0
            columns = ['code','ast', 'aast']
            final_df = pd.DataFrame(columns=columns)
            self.loadVocabulary()
            print("The length of AST Vocabolary: %d" % len(self.ast_vol))
            print("The length of AAST Vocabolary: %d" % len(self.aast_vol))
            while(True):
                  check = os.path.join(self.data_path, "%s_df_%d" % (self.source_name,
                                                                   df_count))
                  if df_count > limit:
                        break
                  if os.path.isfile(check):
                        df = pd.read_pickle(check)
                        self.experiments(df)
                        #final_df = final_df.append(df, ignore_index = True)
                        df_count += 1
                  else:
                        break
            #return final_df

      def loadVocabulary(self):
            file_name = os.path.join(self.data_path, "%s_ast_vol" % self.source_name)
            file_name2 = os.path.join(self.data_path, "%s_aast_vol" % self.source_name)
            self.ast_vol = pd.read_pickle(file_name)
            self.aast_vol = pd.read_pickle(file_name2)

      def experiments(self, df):
            """Show the summaries of a df."""
            length = len(df.index)
            dup_df_j = df[df.duplicated(['ast'])]  # Android
            dup_df_a = df[df.duplicated(['aast'])]  # Java
            print("The length of dataframe: %d " % length)
            print("Checking the number of duplicated in each model. Lower number means less detail.")
            print("Higher number means more detail.")
            print("   Number of duplicated JAVA-ast: %d " % len(dup_df_j))
            print("   Number of duplicated Android-ast: %d"  % len(dup_df_a))

ac = astComparison("aosp_v9_dev")
final_df = ac.dataLoad(2)

