import hashlib
import javalang
import os
import pickle
import sys
import time
from time import perf_counter
from tqdm import tqdm
from typing import Union
# For machine learning
import pandas as pd
import numpy as np
# Customize
sys.path.append("../src/")
import android_tokenizer
from javalang.tokenizer import Annotation, Separator, String, Operator

class Utilization(object):
    """Utilization class."""
    def hashStr(self, inp):
        """Provide md5 hash for a string."""
        hash_object = hashlib.md5(inp.encode('utf-8'))
        return hash_object.hexdigest()

    def loadPickle(self, path):
        """Load pickle file from path."""
        if os.path.isfile(path):
            with open(path, 'rb') as f:
                return pickle.load(f)
        return None

    def storePickle(self, path, data):
        """Store data to pickle file."""
        with open(path, 'wb') as f:
            pickle.dump(data, f)

    def storeJson(self, path, data):
        with open(path, 'w') as fp:
            json.dump(data, fp)

    def loadJson(self, path):
        with open(path, 'r') as fp:
            data = json.load(fp)
        return data


class dataGenerator(object):
    """Generate data from aosp source or android-based java sources"""

    def __init__(self):
        """Initialization."""
        if not os.path.isdir("databases"):
            os.mkdir("databases")
        self.walk_bkup = "databases/"
        if not os.path.isdir("data"):
            os.mkdir("data")
        self.data_out = "data/"
        self.src_out = "src/"
        self.ul = Utilization()
        self.ast_vocabulary = {}
        self.aast_vocabulary = {}
        self.source_code = {}
        if not os.path.isdir("raw_data"):
            os.mkdir("raw_data")
        self.raw_data = "raw_data/"
        self.validate = ['BasicType', 'Keyword', 'Operator',
                         'Separator', 'Modifier', 'Annotation'
                         'Identifier', 'AndroidIdentifier']

    def walk(self, d, ext=None):
        """Walking through directory to get files with extension."""
        output = None
        h = self.ul.hashStr(d)
        output = self.ul.loadPickle(os.path.join(self.walk_bkup, h))
        if output:
            return output
        output = []
        for root, dirs, files in os.walk(d):
            for file in files:
                if not ext:
                    output.append(os.path.join(root, file))
                else:
                    if not file.endswith(ext):
                        continue
                    output.append(os.path.join(root, file))
        # store
        self.ul.storePickle(os.path.join(self.walk_bkup, h), output)
        return output

    def code_to_ast(self, tokens, ast=0):
        ast_line = ""
        if tokens:
            for t in tokens:
                name = type(t).__name__
                val = t.value
                if name not in self.validate:
                    name = "UNK"
                token = "___%s--%s" % (name, val)
                ast_line += token
                # We will care about the vocabulary later
                if ast:
                    if token not in self.ast_vocabulary:
                        token_num = len(self.ast_vocabulary)
                        self.ast_vocabulary[token] = token_num
                else:
                    # aast
                    if token not in self.aast_vocabulary:
                        token_num = len(self.aast_vocabulary)
                        self.aast_vocabulary[token] = token_num
                #     # Make source code references
        ast_line += "<eos>"
        return ast_line

    def parse(self, file):
        """Turn file into AST type, expected to be java file."""
        # Prepare pandas dataframe
        try:
            with open(file, "r") as f:
                source = f.readlines()
        except Exception:
            return None
        data = []
        for line in source:
            try:
                if line.lstrip().startswith("/") or \
                    line.lstrip().startswith("*") or  \
                    line.lstrip().startswith("@") or  \
                    line.lstrip().startswith("#"):
                    continue
                if line == "\n":
                    continue
                aast_tokens = list(android_tokenizer.tokenize(line))
                ast_line = self.code_to_ast(aast_tokens, 1)
                ast_tokens = list(javalang.tokenizer.tokenize(line))
                aast_line = self.code_to_ast(ast_tokens, 0)
                data.append([line, aast_line, ast_line])
            except Exception:
                continue
        # store to dataframe
        return data

    def storeDataSample(self, source_name, f, token_src):
        """Store data sample."""
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            os.makedirs(out_folder)
        file_hash = self.ul.hashStr(f)
        out_file = os.path.join(out_folder, file_hash)
        self.ul.storePickle(out_file, token_src)

    def storeDataSource(self, source_name, f, token_src):
        """Store data sample."""
        out_folder = os.path.join(os.path.join(self.src_out, source_name))
        if not os.path.isdir(out_folder):
            os.makedirs(out_folder)
        file_hash = self.ul.hashStr(f)
        out_file = os.path.join(out_folder, file_hash)
        self.ul.storePickle(out_file, token_src)

    def checkDataSample(self, source_name, f):
        """check data sample."""
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            return None
        file_hash = self.ul.hashStr(f)
        out_file = os.path.join(out_folder, file_hash)
        try:
            return self.ul.loadPickle(out_file)
        except Exception:
            return None

    def storeVocabulary(self, source_name):
        """Store data sample."""
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            os.makedirs(out_folder)
        file_name = "%s_ast_vol" % source_name
        file_name2 = "%s_aast_vol" % source_name
        out_file = os.path.join(out_folder, file_name)
        out_file2 = os.path.join(out_folder, file_name2)
        self.ul.storePickle(out_file, self.ast_vocabulary)
        self.ul.storePickle(out_file2, self.aast_vocabulary)

    def loadVocabularyFromFile(self, source_name):
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            return False
        file_name = os.path.join(os.path.join(self.data_out, source_name), "%s_ast_vol" % source_name)
        file_name2 = os.path.join(os.path.join(self.data_out, source_name), "%s_aast_vol" % source_name)
        self.ast_vocabulary = self.ul.loadPickle(file_name)
        self.aast_vocabulary = self.ul.loadPickle(file_name2)

    def loadVocabulary(self, source_name):
        """Load vocabulary of source code."""
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            return set()
        file_name = "%s_vocabulary" % source_name
        out_file = os.path.join(out_folder, file_name)
        self.vocabulary = self.ul.loadPickle(out_file)

    def convertPicklesToText(self, source_name):
        out_folder = os.path.join(os.path.join(self.data_out, source_name))
        if not os.path.isdir(out_folder):
            return None
        raw_src_folder = os.path.join(self.raw_data, source_name)
        if not os.path.isdir(raw_src_folder):
            os.makedirs(raw_src_folder)
        for f in os.listdir(out_folder):
            out_file = os.path.join(out_folder, f)
            sources = self.ul.loadPickle(out_file)
            raw_out = os.path.join(self.raw_data, source_name, f)
            with open(raw_out, 'w') as f:
                for item in sources:
                    f.write("%s\n" % item)

    def summary(self, source_name, df, time, total_file):
        out_folder = os.path.join("data", source_name)
        dup_df_j = df[df.duplicated(['ast'])]
        dup_df_a = df[df.duplicated(['aast'])]
        with open(os.path.join(out_folder, "summary.txt"), "w") as f:
            f.write("SUMMARY:\n")
            f.write("Execution Time: %s\n" % time)
            f.write("Total Files: %d\n" % total_file)
            f.write("AST Vocabolary length: %d\n"  % len(self.ast_vocabulary))
            f.write("AAST Vocabolary length: %d\n" % len(self.aast_vocabulary))
            f.write("Number of duplicated JAVA-ast: %d\n" % len(dup_df_j))
            f.write("Number of duplicated Android-ast: %d\n"  % len(dup_df_a))
            f.write("Data Length: %d\n" % len(df.index))

class dataAnalysis(object):
    def __init__(self, vocab_file, target_file, data_file):
        self.vocab_file = vocab_file
        self.target_file = target_file
        self.data_file = data_file # AST files
        self.dg = dataGenerator()

    def distanceCalculate(self):
        # convert the target file into AST
        target_ast = self.dg.parse(self.target_file)

# For source code generations
def processSourceData_step1():
    source_path = "/home/tucn/Workspace/aosp/packages/apps/"  # This is enough
    source_name = "aosp_v10_2"
    # source_path = "unittest_samples/src"
    # source_name = "unittest"
    out_folder = os.path.join("data", source_name)
    if not os.path.isdir(out_folder):
        os.mkdir(out_folder)
    dg = dataGenerator()
    out = dg.walk(source_path, '.java')
    df_index = 0
    #saved = os.path.join("data", os.path.join(source_name, "df_%s" % df_index))
    #df = pd.read_pickle(saved)
    #with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    #   print(df)
    #return
    data_frames = []
    t0= perf_counter()
    file_count = 0
    for i in tqdm(range(len(out))):
        f = out[i]
        if "/src/" in f and "/out/" not in f:
            file_count += 1
            ret = dg.parse(f)
            columns = ['code','ast', 'aast']
            df_ = pd.DataFrame(ret, columns=columns)
            data_frames.append(df_)
            final_df = pd.concat(data_frames)
            # Store to pandas if is too much
            if len(final_df.index) > 30000:
                final_df.to_pickle(os.path.join("data", os.path.join(source_name, "%s_df_%s" % (source_name, df_index))))
                final_df = final_df.iloc[0:0]
                data_frames = []
                df_index += 1
        #with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #print(final_df)
    # Store vocabulary
    t1 = perf_counter() - t0
    dg.storeVocabulary(source_name=source_name)
    dg.summary(source_name, final_df, t1, file_count)

def mergeSourceData_step2():
    print("Merge source data to data.txt")
    data = set()
    source_name = "aosp_v9_dev"
    source_path = os.path.join("raw_data", source_name)
    source_dir = os.listdir(source_path)
    for i in range(len(source_dir)):
        f_path = os.path.join(source_path, source_dir[i])
        set_lines = set(line.rstrip('\n') for line in open(f_path))
        data |= set_lines
        print("Number of lines: %d" % len(data))
    output = os.path.join("databases", "%s_data.txt" % source_name)
    with open(output, 'w') as f:
        for line in data:
            f.write(line)
    print("Done!")


# vocab_file = "/Users/tucn/Devs/git_projects/droidgen_error_correction/dataset/custom/aosp_v9_0_vocabulary.txt"
# data_file = "/Users/tucn/Devs/git_projects/droidgen_error_correction/dataset/custom/data.txt"
# target_file = "/Users/tucn/Documents/Documents/Paper/DatasetsGenerator/trans_out/demo/cfe69ecf849cd35b8797caba9a39ddba"
# da = dataAnalysis(vocab_file=vocab_file, data_file=data_file, target_file=target_file)
# da.distanceCalculate()
processSourceData_step1()
#mergeSourceData_step2()
