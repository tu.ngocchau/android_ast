# EXPERIMENTS

This folder includes experiment output between AAST and java AST

Objective:

 1. Effectiveness: 
     + Summary of new AAST (size of dict, project-AOSP_v9, LOC before-after compressed to AST) 
     + Error rate (change from AST-> code and code -> AST)
         - Change code to AST and AAST --> store original code
         - Let the machine learning algorithm (supervised learning or LSTM) guessing the code
         - Compare error rate between AST and AAST

 2. Disadvantage
     + FP can be occurs if a variable has the same name with Android Identifier.
         + Decrease FP by using case-insensitive filter
      
Extend for this research:
    - Deep AAST-based Learning for Android
    - Signature-based Malware Analysis
